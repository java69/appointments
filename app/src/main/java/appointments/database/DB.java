package appointments.database;

import java.sql.*;
//import java.sql.Statement;
//import java.sql.Connection;
//import java.sql.SQLException;

public class DB{

	public static Statement getInstance(){
		final String DRIVER = "com.mysql.jdbc.Driver";
		final String CONNECTION = "jdbc:mysql://localhost/AccountDatabase;create=true";

		try {
			Class.forName(DRIVER).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try{
			Connection conn = DriverManager.getConnection(CONNECTION);
			Statement stmt = conn.createStatement();
			return stmt;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
