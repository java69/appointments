package appointments.appointmentClasses;

import appointments.database.*;
import java.sql.Statement;

public class Appointment{
	private String hour;
	private String minute;
	private String date;
	private String day;
	private String month;
	private String year;

	public Appointment setHour(String hour){
		this.hour = hour;
		return this;
    }

	public Appointment setMinute(String minute){
		this.minute = minute;
		return this;
    }

	public Appointment setDate(String date){
		this.date = date;
		return this;
    }
	public Appointment setDay(String day){
		//TODO: figure out way to auto set this based on otehr input
		this.day = day;
		return this;
    }

	public Appointment setMonth(String month){
		this.month = month;
		return this;
    }

	public Appointment setYear(String year){
		this.year = year;
		return this;
    }

	public String getHour(){
		return this.hour;
    }

	public String getMinute(){
		return this.minute;
    }

	public String getDate(){
		return this.date;
    }

	public String getDay(){
		return this.day;
    }

	public String getMonth(){
		return this.month;
    }

	public String getYear(){
		return this.year;
    }

	public boolean isEqual(Appointment other){
		if(!this.date.equalsIgnoreCase(other.getDate()))
		    return false;

		if(!this.hour.equalsIgnoreCase(other.getHour()))
		    return false;
		
		if(!this.minute.equalsIgnoreCase(other.getMinute()))
		    return false;
		
		if(!this.year.equalsIgnoreCase(other.getYear()))
			return false;

		return true;
	}

	public int save(){
		Statement db = DB.getInstance();
		try {
			int res = db.executeUpdate("INSERT INTO appointment SET date='09'");
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

	public String toString(){
		return "Day: " + this.day + "\n" +
			"Month: " + this.month + "\n" +
			"Date: " + this.date + "\n" +
			"Time: " + this.hour + ":" + this.minute + "\n" +
			"Year: " + this.year;
	}

}
